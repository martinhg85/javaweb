/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.application.Model;
import org.entities.Post;

/**
 *
 * @author Martín
 */
public class PostsModel extends Model {

    public PostsModel() {
        super("posts");//utiliza la tabla posts
    }

    public ArrayList<Post> obtenerPosts() {
        ArrayList<Post> posts = new ArrayList<Post>();
        try {

            Statement stm = this.db.getConnection().createStatement();
            String query = "SELECT * FROM posts";
            ResultSet rs = stm.executeQuery(query);

            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"), rs.getString("texto")));
            }

        } catch (SQLException ex) {
            System.out.println("error SQL");
        }


        return posts;
    }

    public boolean insertarPost(Post post) {

        try {
            String query = "INSERT INTO POSTS (usuario_id,texto) VALUES (0,?)";
            PreparedStatement stm = this.db.getConnection().prepareStatement(query);
            stm.setString(1, post.getTexto());
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }





    }
}
