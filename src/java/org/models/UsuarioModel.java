/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.models;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.application.Model;
import org.entities.Post;
import org.entities.Usuario;

/**
 *
 * @author Martín
 */
public class UsuarioModel extends Model {

    public UsuarioModel() {
        super("usuarios");//utiliza la tabla usuarios
    }

    public ArrayList<Usuario> obtenerUsuarios() {
        ArrayList<Usuario> uss = new ArrayList<Usuario>();
        try {

            Statement stm = this.db.getConnection().createStatement();
            String query = "SELECT * FROM usuario";
            ResultSet rs = stm.executeQuery(query);

            while (rs.next()) {
                uss.add(new Usuario(rs.getInt("id"),
                            rs.getString("username"),
                            rs.getString("password"),
                            rs.getString("nombre"),
                            rs.getString("apellido")));
            }

        } catch (SQLException ex) {
            System.out.println("error SQL");
        }


        return uss;
    }

    public boolean insertarPost(Post post) {

        try {
            String query = "INSERT INTO POSTS (usuario_id,texto) VALUES (0,?)";
            PreparedStatement stm = this.db.getConnection().prepareStatement(query);
            stm.setString(1, post.getTexto());
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }





    }
    
     public boolean validarUsuario(Usuario user, HttpSession session) {
         
        try {
            String query = "SELECT * FROM usuarios WHERE username=?";
            PreparedStatement stm = this.db.getConnection().prepareStatement(query);
            stm.setString(1, user.getUserName());
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                if (rs.getString("password").equals(user.getPass())) { 
                    // Usuario Valido!
                    session.setAttribute("username",user.getUserName());
                    return true;
                } 
            }
            return false;
        } catch (SQLException e) {
            return false;
        }





    }
}
