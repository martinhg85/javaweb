/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martín
 */
public class Database {
    private String driver="com.mysql.jdbc.Driver";
    private String server="localhost";
    private String user="root";
    private String pass="";
    private String dbName="redsocial";
    private Connection connection;
    
    public Database() {
        try {
            Class.forName(driver);
            //connect();
        } catch (ClassNotFoundException ex) {
            
        }
    }  
   
    public void connect(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://"+server+"/"+dbName, user, pass);
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void close(){
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    public Connection getConnection() {
        return connection;
    }
   
    
}
