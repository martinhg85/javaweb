/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entities;

/**
 *
 * @author Martín
 */
public class Post {
    private int id;
    private String texto;

    public Post() {
    }

    public Post(String texto) {
            this.texto = texto;
    }
    
    public Post(int id, String texto) {
        this.id = id;
     
        this.texto = texto;
    }

    

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the titulo
     */


    /**
     * @param titulo the titulo to set
     */

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    
    
}
