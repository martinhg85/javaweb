/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entities;

/**
 *
 * @author Martín
 */
public class Usuario {
    private int id;
    private String userName;
    private String pass;
    private String nombre;
    private String apellido;

    public Usuario() {
    }

    public Usuario(int id, String userName, String pass, String nombre, String apellido) {
        this.id = id;
        this.userName = userName;
        this.pass = pass;
        this.nombre = nombre;
        this.apellido = apellido;
    }
    public Usuario( String userName, String pass, String nombre, String apellido) {
        this.userName = userName;
        this.pass = pass;
        this.nombre = nombre;
        this.apellido = apellido;
    }
    
      public Usuario( String userName, String pass) {
        this.userName = userName;
        this.pass = pass;
    }

    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

   
    
    
}
